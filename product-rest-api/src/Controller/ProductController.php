<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

#[Route('/api', name: 'api_')]
class ProductController extends AbstractController
{

    #[Route('/products', name: 'product_index', methods:['get'] )]
    public function index(ManagerRegistry $doctrine, ProductRepository $productRepository): JsonResponse
    {
        $products = $productRepository->findAll();
   
        $data = [];
        
        foreach ($products as $product) {
            $categoryList = [];

            foreach($product->getCategory() as $category){
                $categoryList[] = $category->getName();
            }
           $data[] = [
               'id' => $product->getId(),
               'name' => $product->getName(),
               'price' => $product->getPrice(),
               'quantity' => $product->getQuantity(),
               'image' => $product->getImage(),
               'category' => $categoryList,
           ];
        }
   
        return $this->json($data);
    }
 
 
    #[Route('/product', name: 'product_create', methods:['post'] )]
    public function create(ManagerRegistry $doctrine, Request $request, CategoryRepository $categoryRepository): JsonResponse
    {
        $entityManager = $doctrine->getManager();

        $product = new Product();

        foreach ($request->request->all()['category'] as $category){
             $product->addCategory($categoryRepository->findOneBy(["id" => $category]));
         }

        $product->setName($request->request->get('name'));
        $product->setPrice($request->request->get('price'));
        $product->setQuantity($request->request->get('quantity'));
        $product->setImage($request->request->get('image'));

        $categoryList = [];

        foreach($product->getCategory() as $category){
            $categoryList[] = $category->getName();
        }
   
        $entityManager->persist($product);
        $entityManager->flush();
        $data =  [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'quantity' => $product->getQuantity(),
            'image' => $product->getImage(),
            'category' => $categoryList,
        ];
           
        return $this->json($data);
    }
 
 
    #[Route('/products/{id}', name: 'product_show', methods:['get'] )]
    public function show(ManagerRegistry $doctrine, int $id, ProductRepository $productRepository): JsonResponse
    {
        $product = $productRepository->find($id);
   
        if (!$product) {
   
            return $this->json('Pas de produits trouvé pour l\'Id : ' . $id, 404);
        }
   
        $categoryList = [];

        foreach($product->getCategory() as $category){
            $categoryList[] = $category->getName();
        }

        $data =  [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'quantity' => $product->getQuantity(),
            'image' => $product->getImage(),
            'category' => $categoryList,
        ];
           
        return $this->json($data);
    }
 
    #[Route('/products/{id}', name: 'product_update', methods:['post'] )]
    public function update(ManagerRegistry $doctrine, Request $request, int $id, ProductRepository $productRepository, CategoryRepository $categoryRepository): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $product = $productRepository->find($id);
   
        if (!$product) {
            return $this->json('Pas de produits trouvé pour l\'Id : ' . $id, 404);
        }

        foreach ($request->request->all()['category'] as $category){
            $product->addCategory($categoryRepository->findOneBy(["id" => $category]));
        }

        $product->setName($request->request->get('name'));
        $product->setPrice($request->request->get('price'));
        $product->setQuantity($request->request->get('quantity'));
        $product->setImage($request->request->get('image'));
        $entityManager->flush();

        $categoryList = [];

        foreach($product->getCategory() as $category){
            $categoryList[] = $category->getName();
        }
   
        $data =  [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'quantity' => $product->getQuantity(),
            'image' => $product->getImage(),
            'category' => $categoryList,
        ];
           
        return $this->json($data);
    }
 
    #[Route('/product/{id}', name: 'product_delete', methods:['delete'] )]
    public function delete(ManagerRegistry $doctrine, int $id, ProductRepository $productRepository): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $product = $productRepository->find($id);
   
        if (!$product) {
            return $this->json('Pas de produit trouvé pour l\'id : ' . $id, 404);
        }
   
        $entityManager->remove($product);
        $entityManager->flush();
   
        return $this->json('Produit supprimé : ' . $id);
    }
}
