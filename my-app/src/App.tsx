import React from 'react';
import Header from './components/Header/Header';
 import ProductList from './components/read-products.component';
import AddProduct from './components/create-product.component';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';

function App() {
  return (
    <>
    <Router>
        <Header />
        <Routes>

  <Route path="/" element={<ProductList />} /> 
  <Route path="/add" element={<AddProduct />} />
  </Routes>
</Router>
    </>
  );
}

export default App;
