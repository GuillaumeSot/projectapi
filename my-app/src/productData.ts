export default interface IProductData {
    id : number,
    name: string,
    price: number,
    quantity: number,
    image: string,
    category : any
  }