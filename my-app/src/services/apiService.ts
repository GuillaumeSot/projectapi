import http from "../url";
import IProductData from "../productData";

class ProductDataService {
  getAll() {
    return http.get<Array<IProductData>>("/products");
  }

  get(id: string) {
    return http.get<IProductData>(`/products/${id}`);
  }

  create(data: IProductData) {
    return http.post<IProductData>("/product", data);
  }

  update(data: IProductData, id: any) {
    return http.put<any>(`/products/${id}`, data);
  }

  delete(id: any) {
    return http.delete<any>(`/product/${id}`);
  }

}

export default new ProductDataService();