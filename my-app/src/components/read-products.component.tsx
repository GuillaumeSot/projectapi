import { Component, ChangeEvent } from "react";
import ProductDataService from "../services/apiService";
import { Link } from "react-router-dom";
import IProductData from "../productData";

type Props = {};

type State = {
  products: Array<IProductData>,
  currentProduct : IProductData | null,
  currentIndex : number,
};
export default class ProductList extends Component<Props, State>{
    constructor(props: Props) {
      super(props);
      this.getAllProduct = this.getAllProduct.bind(this);
  
      this.state = {
        products: [],
        currentProduct : null,
        currentIndex : -1,
      };
    }

    componentDidMount() {
        this.getAllProduct();
      }

getAllProduct() {
    ProductDataService.getAll()
      .then((response: any) => {
        this.setState({
          products: response.data
        });
        console.log(response.data);
      })
      .catch((e: Error) => {
        console.log(e);
      });
  }
  setCurrentProduct(product: IProductData, index: number) {
    this.setState({
      currentProduct: product,
      currentIndex: index
    });
  }


  render() {
    const { products,currentProduct , currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>Liste de produits</h4>

          <ul className="list-group">
            {products &&
              products.map((product: IProductData, index: number) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setCurrentProduct(product, index)}
                  key={index}
                >
                  {product.name}
                  
                </li>
              ))}
          </ul>


        </div>
        <div className="col-md-6">
          {currentProduct ? (
            <div>
              <h4>Produit</h4>
              <div>
                <label>
                  <strong>Nom:</strong>
                </label>{" "}
                {currentProduct.name}
              </div>
              <div>
                <label>
                  <strong>Prix:</strong>
                </label>{" "}
                {currentProduct.price}
              </div>
              <div>
                <label>
                  <strong>Quantité:</strong>
                </label>{" "}
                {currentProduct.quantity }
            
              </div>

              <div>
                <label>
                  <strong>Catégories: </strong>
                </label>{" "}
                {currentProduct.category.forEach(function (value : any) {
                 console.log(value)

                
    }) }
            
              </div>
              

              <Link
                to={"/products/" + currentProduct.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>clique</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}