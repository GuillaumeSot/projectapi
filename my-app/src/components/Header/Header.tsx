import React from 'react';
import styles from './Header.module.css';

const data = {
    "links": [
        {
            "label": "Page d'accueil",
            "href": "/"
        },
        {
            "label": "Ajouter",
            "href": "/add"
        },
        {
            "label": "ProduitInfo",
            "href": "/product"
        }
    ]
}
const linksString = JSON.stringify(data);
const links = JSON.parse(linksString).links;

type Link = {
    label: string;
    href: string;
};

const Links: React.FC<{ links: Link[] }> = ({ links }) => {
    return (
        <div className={styles['links-container']}>
            {links.map((link: Link) => {
                return (
                    <div key={link.href} className={styles['link']}>
                        <a href={link.href}>
                            {link.label}
                        </a>
                    </div>
                )
            })}
        </div>
    )
};

const Header: React.FC<{}> = () => {
    return (
        <nav className={styles.navbar}>
            <div className="">
                <span>Gestion de produit</span>
            </div>
            <Links links={links} />
        </nav>
    )
}

export default Header;