import { Component, ChangeEvent } from "react";
import ProductDataService from "../services/apiService";
import IProductData from '../productData';

type Props = {};

type State = IProductData & {
  submitted: boolean
};

export default class AddProduct extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangePrice = this.onChangePrice.bind(this);
    this.onChangeQuantity = this.onChangeQuantity.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.saveProduct = this.saveProduct.bind(this);
    this.newProduct = this.newProduct.bind(this);

    let category = [1,2];
    this.state = {
      id : 0,
      name: "",
      price: 0,
      quantity: 0,
      image:"",
      category : category ,
      submitted: false
    };
  }

  onChangeName(e: ChangeEvent<HTMLInputElement>) {
    this.setState({
      name: e.target.value
    });
  }

  onChangePrice(e: ChangeEvent<HTMLInputElement>) {
    this.setState({
      price: Number(e.target.value)
    });
  }
  
  onChangeQuantity(e: ChangeEvent<HTMLInputElement>) {
    this.setState({
      quantity: Number(e.target.value)
    });
  }

  onChangeImage(e: ChangeEvent<HTMLInputElement>) {
    this.setState({
      image: e.target.value
    });
  }
  saveProduct() {
    const data: IProductData = {
      id: 0,
      name: this.state.name,
      price: this.state.price,
      quantity : this.state.quantity,
      image: this.state.image,
      category: this.state.category
    };

    ProductDataService.create(data)
      .then((response: any) => {
        this.setState({
          id: 0,
          name: response.data.name,
          price: response.data.price,
          quantity: response.data.quantity,
          image: response.data.image,
          submitted: true
        });
        console.log(response.data);
      })
      .catch((e: Error) => {
        console.log(e);
      });
  }

  newProduct() {
    this.setState({
      id: 0,
      name: "",
      price: 0,
      quantity: 0,
      image:"",
      category : [],
      submitted: false
    });
  }

  render() {
    const { submitted, name, price, quantity,image,category } = this.state;

    return (
      <div className="submit-form">
        {submitted ? (
          <div>
            <h4>Produit ajouté avec succès ! </h4>
            <button className="btn btn-success" onClick={this.newProduct}>
              Ajouter
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="name">Nom</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={name}
                onChange={this.onChangeName}
                name="name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="price">Prix</label>
              <input
                type="text"
                className="form-control"
                id="price"
                required
                value={price}
                onChange={this.onChangePrice}
                name="price"
              />
            </div>

            <div className="form-group">
              <label htmlFor="quantity">Quantité</label>
              <input
                type="text"
                className="form-control"
                id="quantity"
                required
                value={quantity}
                onChange={this.onChangeQuantity}
                name="quantity"
              />
            </div>

            <div className="form-group">
              <label htmlFor="image">Image</label>
              <input
                type="text"
                className="form-control"
                id="image"
                required
                value={image}
                onChange={this.onChangeImage}
                name="image"
              />
            </div>

            <button onClick={this.saveProduct} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}